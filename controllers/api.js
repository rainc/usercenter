/**
 * Created by Rainc on 2016/3/1.
 */

var fs = require('fs');
var mysqlApi = require('./basedao.js');
var crypto = require('crypto');
var exec = require('child_process').exec;
var tool = require('./tool.js');
var weiXinApi = require('./weiXin');

var primaryViewCb = function(title,ret){
  return {layout:'primary.html',title:title, ret: JSON.stringify(ret)}
}

module.exports  = {
  getCheckUserBind: function(req,res){
    res.render('user/checkUserBind.html',primaryViewCb('帐号绑定',''))
  },
  getUserBind: function(req,res){
    res.render('user/userBind.html',primaryViewCb('帐号绑定',''))
  },
  postUserBind: function(req,res){

  },
  getRegister: function(req,res){
    res.render('user/register.html',primaryViewCb('新用户注册',''))
  },
  postRegister: function(req,res){

  },
  weiXinGetMenus: function(req,res){
    weiXinApi.getMenus(function(err,data){
      if(err){
        logger.error(err);
        res.json({success:false,result:'',errMsg:''})
      }else{
        console.log(JSON.stringify(data));
        if(data.errcode){
          res.json({success:false,result:'',errMsg:'Errcode:' + data.errcode + ',Errmsg:'+ data.errmsg})
        }else{
          res.json({success:true,result:data,errmsg:''})
        }
      }
    })
  },
  weiXinCreateMenus: function(req,res){
    weiXinApi.createMenus(function(err,data){
      if(err){
        logger.error(err);
        res.json({success:false,result:'',errMsg:''})
      }else{
        console.log(JSON.stringify(data));
        if(data.errcode && data.errcode != 0){
          res.json({success:false,result:'',errMsg:'Errcode:' + data.errcode + ',Errmsg:'+ data.errmsg})
        }else{
          res.json({success:true,result:'',errmsg:''})
        }
      }
    })
  }
}




