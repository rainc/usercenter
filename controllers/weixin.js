/**
 * Created by Rainc on 2016/3/4.
 */

var weiXinApi = require('node-weixin-api');
var weiXinAuth = require("node-weixin-auth");
var weiXinConfig = require("node-weixin-config");
var weiXinSettings = require('node-weixin-settings');

var weiXinApp = {
  id: config.isForTest ? config.weiXinTestAppId : config.weiXinAppId  ,
  secret: config.isForTest ? config.weiXinTestAppSECRET : config.weiXinAppSECRET,
  token: config.isForTest ? config.weiXinTestAppToken : config.weiXinAppToken
}
weiXinConfig.app.init(weiXinApp);

module.exports = {
  getMenus: function(cb){
    weiXinApi.menu.get(weiXinApp,function(err,data){
      cb(err,data);
    })
  },
  createMenus: function(cb){
    var menu = {
      "button": [
        {
          "name": "Mos",
          "sub_button": [
            {
              "type": "view",
              "name": "地       图",
              "url": "http://www.i99yun.com/home/map",
              "sub_button": []
            },
            {
              "type": "view",
              "name": "工程列表",
              "url": "http://www.i99yun.com/mosweb",
              "sub_button": []
            },
            {
              "type": "view",
              "name": "预警信息",
              "url": "http://www.i99yun.com/home/alarm",
              "sub_button": []
            }
          ]
        },
        {
          "name": "我",
          "sub_button": [
            {
              "type": "view",
              "name": "我的设置",
              "url": "http://www.i99yun.com/mosweb",
              "sub_button": []
            },
            {
              "type":"view",
              "name":"绑定帐号",
              "url":"http://www.i99yun.com/checkUserBind",
              "sub_button": []
            },
            {
              "type": "view",
              "name": "工程成员",
              "url": "http://www.i99yun.com/mosweb",
              "sub_button": []
            },
            {
              "type": "view",
              "name": "微信授权",
              "url": "http://www.i99yun.com/mosweb",
              "sub_button": []
            }
          ]
        }
      ]
    };
    weiXinApi.menu.create(weiXinApp,menu,function(err,data){
      cb(err,data);
    })
  }
}