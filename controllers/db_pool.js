/**
 * Created with JetBrains WebStorm.
 * User: rainc
 * Date: 2016/03/01
 * Time: 下午 2:45
 * To change this template use File | Settings | File Templates.
 */
var mysql = require('mysql');

var db_config = {
  host: config.dbHost,
  user: config.dbUser,
  password: config.dbPasswd,
  database: config.dbDatabase,
  port: 3306
};

// 创建连接池
var pool = mysql.createPool(db_config);
var connection;

function getPoolConn(){
  return pool;
}

function handleDisconnect(cb) {
  connection = mysql.createConnection({
    host: config.dbHost,
    user: config.dbUser,
    password: config.dbPasswd,
    database: config.dbDatabase,
    port: 3306
  }); // 创建连接对象

  // 如果断线15s后自动重连
  connection.on('error', function (err) {
    logger.error('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
      setTimeout(function(){
        handleDisconnect(cb);
      },15*1000)
    } else {
    }
  });

  // 连接失败,15s后自动重连
  connection.connect(function (err) {
    if (err) {
      logger.error('error when connecting to db:', err);
      setTimeout(function(){
        handleDisconnect(cb);
      },15*1000)
    } else {
      cb(null, connection);
    }
  });
}

exports.getPoolConn = getPoolConn;
exports.handleDisconnect = handleDisconnect;
