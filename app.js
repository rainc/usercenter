/**
 * Created by Rainc on 2016/3/1.
 */
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routers = require('./routers');
var session = require('express-session');
var partials = require('express-partials');

var app = express();

//设置当前模式为开发模式
app.set('env','development');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false ,limit: '50mb'}));
app.use(cookieParser());
app.use(partials());
app.use(express.static(path.join(__dirname, 'dist/app/public')));
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(session({
  secret: 'usercenter', // 建议使用 128 个字符的随机字符串
  cookie: { maxAge: 6000 * 1000 }
}));

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}else{
  // production error handler
  // no stacktraces leaked to user
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });
}

//路由
routers(app);

module.exports = app;