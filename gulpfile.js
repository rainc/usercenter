/**
 * Created by Rainc on 2016/3/5.
 */

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var eslint = require('gulp-eslint');
var minifycss = require('gulp-minify-css');
var clean = require('gulp-clean');

var cssInject = [
  'public/styles/*.css',
  'bower_components/font-awesome/css/font-awesome.min.css'
];
var jsInject = [
  'public/js/*.js'
];
var fontsInject = [
  'bower_components/font-awesome/fonts/*'
];

gulp.task('minifyjs',function(){
  gulp.src(jsInject)
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(sourcemaps.write('../../../srcmaps/public/js'))
    .pipe(gulp.dest('dist/app/public/js'));
});
gulp.task('minifycss',function(){
  gulp.src(cssInject)
    .pipe(sourcemaps.init())
    .pipe(minifycss())
    .pipe(sourcemaps.write('../../../srcmaps/public/styles'))
    .pipe(gulp.dest('dist/app/public/styles'));
});
gulp.task('minifyfonts',function(){
  gulp.src(fontsInject)
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write('../../../srcmaps/public/fonts'))
    .pipe(gulp.dest('dist/app/public/fonts'));
});
gulp.task('minifyelse',function(){

});
gulp.task('clean', function() {
  return gulp.src(['dist/app','dist/srcmaps'], {read: false})
    .pipe(clean({force: true}));
});
gulp.task('lint',['clean'], function() {
  return gulp.src(['*.js', '!gulpfile.js'])
    .pipe(eslint({extends: 'eslint:recommended'
      , rules: {'no-unused-vars':0
        , 'no-console':0
        , 'no-empty':0
        ,"strict": 0
      }
      , globals: {
        'config':true,
        require:true,
        __dirname:true,
        module: true
      }
      , envs: ['node','browser']
    }))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

// 定义develop任务在日常开发中使用
gulp.task('develop',function(){
  gulp.start('minifycss', 'minifyjs','minifyfonts','minifyelse');
  gulp.watch('public/styles/*.css', ['minifycss']);
  gulp.watch('public/js/*.js', ['minifyjs']);
});

// 定义一个prod任务作为发布或者运行时使用
gulp.task('prod',function(){
  gulp.start('minifycss', 'minifyjs','minifyfonts','minifyelse');
});

gulp.task('default', ['clean','lint'], function() {
  gulp.run('develop');
});