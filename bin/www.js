/**
 * Created by Rainc on 2016/3/1.
 */
/**
 * Module dependencies.
 */
config = require('../etc/config.json');
var app = require('../app');
var http = require('http');
var fs = require('fs');
var db_pool = require('../controllers/db_pool');

var log4js = require('log4js');
var exec = require('child_process').exec;

log4js.configure({
  appenders: [
    { type: 'console' },
    {
      type: 'dateFile',
      filename: config.appLogFile,
      //     maxLogSize: 1024,
      backups: 3,
      pattern: "-yyyy-MM-dd",
      alwaysIncludePattern: false,
      category: 'APPSERVER'
    }
  ],
  replaceConsole: true
});
logger = log4js.getLogger('APPSERVER');
logger.setLevel('INFO');

function runApp(){
  /**
   * Get port from environment and store in Express.
   */
  var port = normalizePort(process.env.PORT || config.webPort);
  app.set('port', port);

  /**
   * Create HTTP server.
   */
  var server = http.createServer(app);

  /**
   * Listen on provided port, on all network interfaces.
   */
  server.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);

  /**
   * Normalize a port into a number, string, or false.
   */
  function normalizePort(val) {
    var port = parseInt(val, 10);
    if (isNaN(port)) {
      // named pipe
      return val;
    }
    if (port >= 0) {
      // port number
      return port;
    }
    return false;
  }

  /**
   * Event listener for HTTP server "error" event.
   */
  function onError(error) {
    if (error.syscall !== 'listen') {
      throw error;
    }
    var bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port;
    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        logger.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        logger.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }

  /**
   * Event listener for HTTP server "listening" event.
   */
  function onListening() {
    console.log('Web server listening on port ' + server.address().port);
    console.log('You can debug app with http://' + config.serverIp + ':' + config.webPort)
  }
}

//检查数据库是否连接上
db_pool.handleDisconnect(function(err,con){
  if(!err && con){
    logger.info('初始化数据库链接成功！');
    //开启web server
    runApp();
  }
})

