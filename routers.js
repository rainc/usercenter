/**
 * Created by Rainc on 2016/3/1.
 */

var api = require('./controllers/api');

module.exports = function (app) {
  app.get(['/','/checkUserBind'],api.getCheckUserBind);
  app.get('/userBind',api.getUserBind);
  app.post('/userBind',api.postUserBind);
  app.get('/register',api.getRegister);
  app.post('/register',api.postRegister);

  //微信业务路由
  app.get('/weiXinGetMenus',api.weiXinGetMenus);
  app.get('/weiXinCreateMenus',api.weiXinCreateMenus);
}